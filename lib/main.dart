import 'package:designs/src/models/layout_model.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
 
import 'package:designs/src/pages/launcher_page.dart';
import 'package:designs/src/pages/launcher_tablet_page.dart';

import 'package:designs/src/theme/theme.dart';

void main() => runApp(
  MultiProvider(
    providers: [
      ChangeNotifierProvider<ThemeChanger>(create: (_) => ThemeChanger(2)),
      ChangeNotifierProvider<LayoutModel>(create: (_) => LayoutModel()),
    ],
    child: MyApp(),
  )
);
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final appTheme = Provider.of<ThemeChanger>(context);

    return MaterialApp(
      theme: appTheme.currentTheme,
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: OrientationBuilder(
        builder: (BuildContext context, Orientation orientation) {

          // print('Orientation $orientation');

          final screenSize = MediaQuery.of(context).size;

          if (screenSize.width > 500) {
            return LauncherTabletPage();
          } else {
            return LauncherPage();
          }

          // return Container(
          //   child: LauncherPage(),
          // );
        },
      ),
    );
  }
}