import 'package:flutter/material.dart';

class SquareAnimatedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _SquareAnimated()
        )
    );
  }
}

class _SquareAnimated extends StatefulWidget {
  @override
  __SquareAnimatedState createState() => __SquareAnimatedState();
}

class __SquareAnimatedState extends State<_SquareAnimated> with SingleTickerProviderStateMixin {

  AnimationController animationController;
  Animation<double> moveRight;
  Animation<double> moveUp;
  Animation<double> moveLeft;
  Animation<double> moveDown;

  @override
  void initState() {
    animationController = new AnimationController(
      vsync: this, duration: Duration(milliseconds: 4500)
    );

    moveRight = Tween(begin: 0.0, end: 100.0).animate(
      CurvedAnimation(parent: animationController, curve: Interval(0, 0.25, curve: Curves.bounceOut))
    );

    moveUp = Tween(begin: 0.0, end: -100.0).animate(
      CurvedAnimation(parent: animationController, curve: Interval(0.25, 0.5, curve: Curves.bounceOut))
    );

    moveLeft = Tween(begin: 0.0, end: -100.0).animate(
      CurvedAnimation(parent: animationController, curve: Interval(0.5, 0.75, curve: Curves.bounceOut))
    );

    moveDown = Tween(begin: 0.0, end: 100.0).animate(
      CurvedAnimation(parent: animationController, curve: Interval(0.75, 1.0, curve: Curves.bounceOut))
    );

    animationController.addListener(() {
      if (animationController.status == AnimationStatus.completed) {
        animationController.reset();
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    animationController.forward();

    return AnimatedBuilder(
      animation: animationController,
      child: _Rectangle(),
      builder: (BuildContext context, Widget rectangleWidget) {
        return Transform.translate(
          offset: Offset(moveRight.value + moveLeft.value, moveUp.value + moveDown.value),
          child: rectangleWidget
        );
      },
    );
  }
}

class _Rectangle extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
    return Container(
       width: 70,
       height: 70,
       decoration: BoxDecoration(
         color: Colors.blue
       ),
     );
   }
}