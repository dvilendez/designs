
import 'dart:math';
import 'dart:ui';

import 'package:designs/src/widgets/radial_progress_widget.dart';
import 'package:flutter/material.dart';

class CircularProgressPage extends StatefulWidget {
  @override
  _CircularProgressPageState createState() => _CircularProgressPageState();
}

class _CircularProgressPageState extends State<CircularProgressPage> with SingleTickerProviderStateMixin {

  AnimationController animationController;
  double percentage = 0.0;
  double newPercentage = 0.0;

  @override
  void initState() {
    
    animationController = new AnimationController(vsync: this, duration: Duration(milliseconds: 800));
    
    animationController.addListener(() {
      // print('Controller value: ${animationController.value}');
      setState(() {
        percentage = lerpDouble(percentage, newPercentage, animationController.value);
      });
    });
    
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh),
        backgroundColor: Colors.pink,
        onPressed: () {

          percentage = newPercentage;
          newPercentage += 10;
          if (newPercentage > 100) {
            newPercentage = 0;
            percentage = 0;
          }
          animationController.forward(from: 0.0);
          setState(() {});
        },
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(5.0),
          width: 300,
          height: 300,
          child: RadialProgressWidget(
            percentage: percentage
          )
        ),
      ),
    );
  }
}

