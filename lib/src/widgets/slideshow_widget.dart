
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

class SlideshowWidget extends StatelessWidget {

  final List<Widget> slides;
  final bool dotsTop;
  final Color primaryColor;
  final Color secondaryColor;
  final double primaryBullet;
  final double secondaryBullet;

  const SlideshowWidget({
    @required this.slides,
    this.dotsTop         = false,
    this.primaryColor    = Colors.blue,
    this.secondaryColor  = Colors.grey,
    this.primaryBullet   = 10,
    this.secondaryBullet = 10
  });

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => new _SlidershowModel(),
      child: SafeArea(
        child: Center(
          child: Builder(
            builder: (BuildContext context) {
              Provider.of<_SlidershowModel>(context).primaryColor = this.primaryColor;
              Provider.of<_SlidershowModel>(context).secondaryColor = this.secondaryColor;
              Provider.of<_SlidershowModel>(context).primaryBullet = this.primaryBullet;
              Provider.of<_SlidershowModel>(context).secondaryBullet = this.secondaryBullet;

              return Column(
                children: <Widget>[
                  if ( this.dotsTop ) 
                    _Dots(this.slides.length),
                  Expanded(
                    child: _Slides( this.slides )
                  ),
                  if ( !this.dotsTop ) 
                    _Dots(this.slides.length),
                ],
              );
            },
          )
        ),
      ),
    );
  }
}

class _Dots extends StatelessWidget {

  final int dotsNumber;

  const _Dots(
    this.dotsNumber
  );


  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 70,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: List.generate(dotsNumber, (index) => _Dot(index)),
      ),
    );
  }
}

class _Dot extends StatelessWidget {

  final int index;

  _Dot(this.index);

  @override
  Widget build(BuildContext context) {

    final slidershowModel = Provider.of<_SlidershowModel>(context);

    final pageViewIndex = slidershowModel.currentPage;
    final primaryColor = slidershowModel.primaryColor;
    final secondaryColor = slidershowModel.secondaryColor;
    final primaryBullet = slidershowModel.primaryBullet;
    final secondaryBullet = slidershowModel.secondaryBullet;

    double dotSize;
    Color dotColor;

    if (pageViewIndex  >= index - 0.5 && pageViewIndex  < index + 0.5) {
      dotColor = primaryColor;
      dotSize = primaryBullet;
    } else {
      dotColor = secondaryColor;
      dotSize = secondaryBullet;
    }

    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      width: dotSize,
      height: dotSize,
      margin: EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
        color: dotColor,
        shape: BoxShape.circle
      ),
    );
  }
}

class _Slides extends StatefulWidget {

  final List<Widget> slides;

  _Slides(this.slides);

  @override
  __SlidesState createState() => __SlidesState();
}

class __SlidesState extends State<_Slides> {

  final pageViewController = new PageController();

  @override
  void initState() {
    super.initState();

    pageViewController.addListener(() {
      Provider.of<_SlidershowModel>(context, listen: false).currentPage = pageViewController.page;
    });
  }

  @override
  void dispose() {
    pageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: PageView(
        controller: pageViewController,
        children: widget.slides.map((slide) => _Slide(slide)).toList()
      ),
    );
  }
}

class _Slide extends StatelessWidget {
  
  final Widget slide;

  const _Slide(this.slide);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      padding: EdgeInsets.all(30),
      child: slide
    );
  }
}

class _SlidershowModel with ChangeNotifier {
  double _currentPage     = 0;
  Color _primaryColor     = Colors.blue;
  Color _secondaryColor   = Colors.grey;
  double _primaryBullet   = 12;
  double _secondaryBullet = 12;

  double get currentPage => this._currentPage;

  set currentPage(double currentPage) {
    this._currentPage = currentPage;
    notifyListeners();
  }

  Color get primaryColor => this._primaryColor;

  set primaryColor(Color primaryColor) {
    this._primaryColor = primaryColor;
  }

  Color get secondaryColor => this._secondaryColor;

  set secondaryColor(Color secondaryColor) {
    this._secondaryColor = secondaryColor;
  }

  double get primaryBullet => this._primaryBullet;

  set primaryBullet(double primaryBullet) {
    this._primaryBullet = primaryBullet;
  }

  double get secondaryBullet => this._secondaryBullet;

  set secondaryBullet(double secondaryBullet) {
    this._secondaryBullet = secondaryBullet;
  }
}