import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PinterestButton {
  final Function onPressed;
  final IconData icon;

  PinterestButton({
    @required this.onPressed,
    @required this.icon
  });


}


class PinterestMenuWidget extends StatelessWidget {

  final bool show;
  final Color backgroundColor;
  final Color activeColor;
  final Color inactiveColor;
  final List<PinterestButton> items;

  PinterestMenuWidget({
    this.show            = true,
    this.backgroundColor = Colors.white,
    this.activeColor     = Colors.black,
    this.inactiveColor   = Colors.blueGrey,
    @required this.items
  });

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => new _MenuModel(),
      child: Builder(
        builder: (BuildContext context) {

          Provider.of<_MenuModel>(context).backgroundColor = backgroundColor;
          Provider.of<_MenuModel>(context).activeColor = activeColor;
          Provider.of<_MenuModel>(context).inactiveColor = inactiveColor;

          return AnimatedOpacity(
            duration: Duration(milliseconds: 250),
            opacity: (show) ? 1 : 0,
            child: _PinterestMenuBackground(
              child:_MenuItems(items)
            ),
          );
        },
      )
    );
  }
}

class _PinterestMenuBackground extends StatelessWidget {

  final Widget child;

  const _PinterestMenuBackground({@required this.child});

  @override
  Widget build(BuildContext context) {

    Color backgroundColor = Provider.of<_MenuModel>(context).backgroundColor;

    return Container(
      child: child,
      width: 250,
      height: 60,
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.all(Radius.circular(100)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black38,
            blurRadius: 10,
            spreadRadius: -5
          )
        ]
      ),
    );
  }
}

class _MenuItems extends StatelessWidget {

  final List<PinterestButton> menuItems;

  const _MenuItems(this.menuItems);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: List.generate(menuItems.length, (i) => _PinterestMenuButton(i, menuItems[i])),
    );
  }
}

class _PinterestMenuButton extends StatelessWidget {

  final int index;
  final PinterestButton item;

  const _PinterestMenuButton(this.index, this.item);

  @override
  Widget build(BuildContext context) {

    final selectedItem = Provider.of<_MenuModel>(context).selectedIndex;
    final activeColor = Provider.of<_MenuModel>(context).activeColor;
    final inactiveColor = Provider.of<_MenuModel>(context).inactiveColor;

    return GestureDetector(
      onTap: () {
        Provider.of<_MenuModel>(context, listen: false).selectedIndex = index;
        item.onPressed();
      },
      behavior: HitTestBehavior.translucent,
      child: Container(
        child: Icon(
          item.icon,
          size: (selectedItem == index) ? 35 : 25,
          color: (selectedItem == index) ? activeColor : inactiveColor,
        ),
      ),
    );
  }
}

class _MenuModel with ChangeNotifier {
  int _selectedIndex     = 0;
  Color _backgroundColor = Colors.white;
  Color _activeColor     = Colors.black;
  Color _inactiveColor   = Colors.blueGrey; 

  int get selectedIndex => this._selectedIndex;

  set selectedIndex (int selectedIndex) {
    this._selectedIndex = selectedIndex;
    notifyListeners();
  }

  Color get backgroundColor => this._backgroundColor;

  set backgroundColor (Color backgroundColor) {
    this._backgroundColor = backgroundColor;
  }

  Color get activeColor => this._activeColor;

  set activeColor (Color activeColor) {
    this._activeColor = activeColor;
  }

  Color get inactiveColor => this._inactiveColor;

  set inactiveColor (Color inactiveColor) {
    this._inactiveColor = inactiveColor;
  }
}