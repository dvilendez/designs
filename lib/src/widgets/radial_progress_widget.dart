
import 'dart:math';

import 'package:flutter/material.dart';

class RadialProgressWidget extends StatefulWidget {

  final double percentage;
  final Color primaryColor;
  final Color secondaryColor;
  final double primaryStrokeWidth;
  final double secondaryStrokeWidth;

  const RadialProgressWidget({
    @required this.percentage,
    this.primaryColor = Colors.blue,
    this.secondaryColor = Colors.grey,
    this.primaryStrokeWidth = 10,
    this.secondaryStrokeWidth = 4
  });

  @override
  _RadialProgressWidgetState createState() => _RadialProgressWidgetState();
}

class _RadialProgressWidgetState extends State<RadialProgressWidget> with SingleTickerProviderStateMixin{

  AnimationController animationController;
  double previousPercentage;

  @override
  void initState() {
    animationController = new AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    previousPercentage = widget.percentage;
    super.initState();
    
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    animationController.forward(from: 0.0);

    final differenceToAnimate = widget.percentage - previousPercentage;
    previousPercentage = widget.percentage;

    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget child) {
        return Container(
          padding: EdgeInsets.all(10.0),
          width: double.infinity,
          height: double.infinity,
          child: CustomPaint(
            painter: _MyRadialProgress(
              (widget.percentage - differenceToAnimate) + (differenceToAnimate * animationController.value),
              widget.primaryColor,
              widget.secondaryColor,
              widget.primaryStrokeWidth,
              widget.secondaryStrokeWidth,
            ),
          ),
        );
      },
    );
  }
}

class _MyRadialProgress extends CustomPainter {

  final double percentage;
  final Color primaryColor;
  final Color secondaryColor;
  final double primaryStrokeWidth;
  final double secondaryStrokeWidth;

  _MyRadialProgress(
    this.percentage,
    this.primaryColor,
    this.secondaryColor,
    this.primaryStrokeWidth,
    this.secondaryStrokeWidth
  );

  @override
  void paint(Canvas canvas, Size size) {

    // final Rect rect = new Rect.fromCircle(
    //   center: Offset(-60,0),
    //   radius: 180
    // );

    // final Gradient gradient = new LinearGradient(
    //   colors: <Color>[
    //     Color(0xffC012FF),
    //     Color(0xff6D05E8),
    //     Colors.red,
    //   ]
    // );

    // completed circle
    final paint     = new Paint()
      ..strokeWidth = secondaryStrokeWidth
      ..color       = Colors.grey
      ..style       = PaintingStyle.stroke;

    Offset center = new Offset(size.width * 0.5, size.height * 0.5);
    double radius = min(size.width * 0.5, size.height * 0.5);

    canvas.drawCircle(center, radius, paint);
    
    // Arc
    final paintArc  = new Paint()
      ..strokeWidth = 10
      ..color       = primaryColor
      // ..shader      =gradient.createShader(rect)
      ..strokeCap   = StrokeCap.round
      ..style       = PaintingStyle.stroke;

    double arcAngle = 2 * pi * (percentage / 100);

    canvas.drawArc(
      Rect.fromCircle(center: center, radius: radius),
      -pi / 2,
      arcAngle,
      false,
      paintArc
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
  
}