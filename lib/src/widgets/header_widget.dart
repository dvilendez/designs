import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HeaderWidget extends StatelessWidget {

  final Color color;

  const HeaderWidget({
    @required this.color
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: CustomPaint(
        painter: _HeaderPainter(this.color),
      ),
    );
  }
}


class _HeaderPainter extends CustomPainter {

  final color;

  _HeaderPainter(this.color);

  @override
  void paint(Canvas canvas, Size size) {

    final Rect rect = new Rect.fromCircle(
      center:  Offset(165.0, 150.0),
      radius: 180
    );

    // final Gradient gradient = new LinearGradient(
    //   colors: <Color>[
    //     Color(0xff6D05E8),
    //     Color(0xffC012FF),
    //     Color(0xff6D05FA),
    //   ],
    //   stops: [
    //     0.4,
    //     0.5,
    //     1.0
    //   ]
    // );


    final paint = new Paint();//..shader = gradient.createShader(rect);
    paint.color = this.color;
    // paint.style = PaintingStyle.stroke;
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 20;
    
    final path = new Path();
    path.lineTo(0, size.height * 0.3);
    path.quadraticBezierTo(size.width * 0.25, size.height * 0.35, size.width * 0.5, size.height * 0.3);
    path.quadraticBezierTo(size.width * 0.75, size.height * 0.25, size.width, size.height * 0.3);
    path.lineTo(size.width, 0);
    // path.moveTo(0, size.height * 0.75);
    // path.quadraticBezierTo(size.width * 0.25, size.height * 0.80, size.width * 0.5, size.height * 0.75);
    // path.quadraticBezierTo(size.width * 0.75, size.height * 0.70, size.width, size.height * 0.75);
    // path.lineTo(size.width, size.height);
    // path.lineTo(0, size.height);

    canvas.drawPath(path, paint);
    
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
  
}


class IconHeader extends StatelessWidget {

  final IconData icon;
  final String title;
  final String subtitle;
  final Color color1;
  final Color color2;

  const IconHeader({
    @required this.icon,
    @required this.title,
    @required this.subtitle,
    this.color1 = Colors.grey,
    this.color2 = Colors.blueGrey
  });

  @override
  Widget build(BuildContext context) {

    final Color withe = Colors.white.withOpacity(0.7);

    return Stack(
      children: <Widget>[
        _IconHeaderBackground(color1: color1, color2: color2),
        Positioned(
          top: -50,
          left: -70,
          child: FaIcon(icon, size: 250, color: Colors.white.withOpacity(0.2),)
        ),
        Column(
          children: <Widget>[
            SizedBox(height: 80, width: double.infinity,),
            Text(title, style: TextStyle(fontSize: 20, color: withe)),
            SizedBox(height: 20),
            Text(subtitle, style: TextStyle(fontSize: 25, color: withe, fontWeight: FontWeight.bold)),
            SizedBox(height: 20),
            FaIcon(icon, size: 80, color: Colors.white)
          ],
        )
      ],
    );
  }
}

class _IconHeaderBackground extends StatelessWidget {

  final Color color1;
  final Color color2;

  const _IconHeaderBackground({
    @required this.color1,
    @required this.color2
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 300,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(80)),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: <Color>[
            color1,
            color2
          ] 
        )
      ),
    );
  }
}