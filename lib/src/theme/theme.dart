import 'package:flutter/material.dart';


class ThemeChanger with ChangeNotifier {

  bool _darkTheme = false;
  bool _customTheme = false;

  ThemeData _currentTheme;

  bool get darkTheme => this._darkTheme;
  bool get customTheme => this._customTheme;
  ThemeData get currentTheme => this._currentTheme;

  ThemeChanger(int theme) {
    switch(theme) {
      case 1: // light
        this._darkTheme    = false;
        this._customTheme  = false;
        this._currentTheme = ThemeData.light().copyWith(
          accentColor: Colors.pink
        );
      break;
        
      case 2: // dark
        this._darkTheme    = true;
        this._customTheme  = false;
        this._currentTheme = ThemeData.dark().copyWith(
          accentColor: Colors.pink
        );
      break;

      case 3: // custom
        this._darkTheme    = false;
        this._customTheme  = true;
      break;
      
      default:
        this._darkTheme    = false;
        this._customTheme  = false;
        this._currentTheme = ThemeData.light();
    }
  }


  set darkTheme(bool darkTheme) {
    this._darkTheme = darkTheme;
    this._customTheme = false;

    if (darkTheme) {
      this._currentTheme = ThemeData.dark().copyWith(
        accentColor: Colors.pink
      );
    } else {
      this._currentTheme = ThemeData.light().copyWith(
        accentColor: Colors.pink
      );
    }

    notifyListeners();
  }

  set customTheme(bool customTheme) {
    this._customTheme = customTheme;
    this._darkTheme = false;

    if (customTheme) {
      this._currentTheme = ThemeData.dark().copyWith(
        accentColor: Color(0xff48A0EB),
        primaryColorLight: Colors.white,
        scaffoldBackgroundColor: Color(0xff16202B),
        textTheme: TextTheme(
          bodyText2: TextStyle(color: Colors.white)
        )
      );
    } else {
      this._currentTheme = ThemeData.light();
    }

    notifyListeners();
  }

}