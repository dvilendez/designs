
import 'package:designs/src/theme/theme.dart';
import 'package:designs/src/widgets/radial_progress_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CircularGraphsPage extends StatefulWidget {
  @override
  _CircularGraphsPageState createState() => _CircularGraphsPageState();
}

class _CircularGraphsPageState extends State<CircularGraphsPage> {

  double percentage = 0.0;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh),
        onPressed: () {
          setState(() {
            percentage += 10.0;
            if (percentage > 100.0) {
              percentage = 0.0;
            }
          });
        },
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              CustomRadialProgress(percentage: percentage, primaryColor: Colors.red,),
              CustomRadialProgress(percentage: percentage * 1.2, primaryColor: Colors.orange),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              CustomRadialProgress(percentage: percentage * 1.4, primaryColor: Colors.deepPurple),
              CustomRadialProgress(percentage: percentage * 1.6, primaryColor: Colors.pink),
            ],
          )
        ],
      )
    );
  }
}

class CustomRadialProgress extends StatelessWidget {

  final double percentage;
  final Color primaryColor;
  
  const CustomRadialProgress({
    @required this.percentage, this.primaryColor,
  });


  @override
  Widget build(BuildContext context) {

    final appTheme = Provider.of<ThemeChanger>(context).currentTheme;

    return Container(
      height: 150,
      width: 150,
      child: RadialProgressWidget(
        percentage: percentage,
        primaryColor: primaryColor,
        secondaryColor: appTheme.textTheme.bodyText2.color,
        primaryStrokeWidth: 10,
        secondaryStrokeWidth: 4
        
      ),
    );
  }
}