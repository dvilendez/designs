
import 'package:designs/src/theme/theme.dart';
import 'package:flutter/material.dart';

import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:designs/src/widgets/pinterest_menu_widget.dart';
import 'package:provider/provider.dart';

class PinterestPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider(
      create: (_) => _MenuModel(),
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            PinterestGrid(),
            _PinterestMenuLocation(),
          ],
        )
      ),
    );
  }
}

class _PinterestMenuLocation extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {

    double widthScreen = MediaQuery.of(context).size.width;
    final show = Provider.of<_MenuModel>(context).show;
    final appTheme = Provider.of<ThemeChanger>(context).currentTheme;

    if (widthScreen > 500) {
      widthScreen = widthScreen - 300;
    }

    return Positioned(
      bottom: 30,
      child: Container(
        width: widthScreen,
        child: Row(
          children: <Widget>[
            Spacer(),
            PinterestMenuWidget(
              show: show,
              backgroundColor: appTheme.scaffoldBackgroundColor,
              activeColor: appTheme.accentColor,
              inactiveColor: Colors.blueGrey,
              items: [
                PinterestButton(icon: Icons.pie_chart, onPressed: () { print('Icon pie_chart'); }),
                PinterestButton(icon: Icons.search, onPressed: () { print('Icon search'); }),
                PinterestButton(icon: Icons.notifications, onPressed: () { print('Icon notifications'); }),
                PinterestButton(icon: Icons.supervised_user_circle, onPressed: () { print('Icon supervised_user_circle'); }),
              ],
            ),
            Spacer(),
          ],
        )
      )
    );
  }
}

class PinterestGrid extends StatefulWidget {

  @override
  _PinterestGridState createState() => _PinterestGridState();
}

class _PinterestGridState extends State<PinterestGrid> {
  final List<int> items = List.generate(200, (index) => index);
  ScrollController controller = new ScrollController();
  double beforeScroll = 0.0;

  @override
  void initState() {
    super.initState();

    controller.addListener(() {
      if (controller.offset < 0) {
        print('Do Nothing!');
      } else if (controller.offset > beforeScroll) {
        print('Hide the menu!');
        Provider.of<_MenuModel>(context, listen: false).show = false;
      } else {
        print('Show the menu!');
        Provider.of<_MenuModel>(context, listen: false).show = true;
      }
      beforeScroll = controller.offset;
    });

  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    int count = 2;
    if (MediaQuery.of(context).size.width > 500) {
      count = 3;
    }


    return new StaggeredGridView.countBuilder(
      controller: controller,
      crossAxisCount: count,
      itemCount: items.length,
      itemBuilder: (BuildContext context, int index) => _PinterestItem(index),
      staggeredTileBuilder: (int index) =>
          new StaggeredTile.count(1, index.isEven ? 1 : 2),
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
    );
  }
}

class _PinterestItem extends StatelessWidget {
  final int index;

  const _PinterestItem(this.index);

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.all(5),
        decoration: BoxDecoration(
          color: Colors.blue,
          borderRadius: BorderRadius.all(Radius.circular(30))
        ),
        child: new Center(
          child: new CircleAvatar(
            backgroundColor: Colors.white,
            child: new Text('$index'),
          ),
        ));
  }
}

class _MenuModel with ChangeNotifier {
  bool _show = true;

  bool get show => this._show;
  set show(bool show) {
    this._show = show;
    notifyListeners();
  }
}