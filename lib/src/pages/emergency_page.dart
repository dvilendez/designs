
import 'package:animate_do/animate_do.dart';
import 'package:designs/src/widgets/fat_button_widget.dart';
import 'package:flutter/material.dart';

import 'package:designs/src/widgets/header_widget.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ItemButton {

  final IconData icon;
  final String texto;
  final Color color1;
  final Color color2;

  ItemButton( this.icon, this.texto, this.color1, this.color2 );
}



class EmergencyPage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {

    bool isLarge = false;
    if (MediaQuery.of(context).size.height > 550) {
      isLarge = true;
    }
    
    final items = <ItemButton>[
      new ItemButton( FontAwesomeIcons.carCrash, 'Motor Accident', Color(0xff6989F5), Color(0xff906EF5) ),
      new ItemButton( FontAwesomeIcons.plus, 'Medical Emergency', Color(0xff66A9F2), Color(0xff536CF6) ),
      new ItemButton( FontAwesomeIcons.theaterMasks, 'Theft / Harrasement', Color(0xffF2D572), Color(0xffE06AA3) ),
      new ItemButton( FontAwesomeIcons.biking, 'Awards', Color(0xff317183), Color(0xff46997D) ),
      new ItemButton( FontAwesomeIcons.carCrash, 'Motor Accident', Color(0xff6989F5), Color(0xff906EF5) ),
      new ItemButton( FontAwesomeIcons.plus, 'Medical Emergency', Color(0xff66A9F2), Color(0xff536CF6) ),
      new ItemButton( FontAwesomeIcons.theaterMasks, 'Theft / Harrasement', Color(0xffF2D572), Color(0xffE06AA3) ),
      new ItemButton( FontAwesomeIcons.biking, 'Awards', Color(0xff317183), Color(0xff46997D) ),
      new ItemButton( FontAwesomeIcons.carCrash, 'Motor Accident', Color(0xff6989F5), Color(0xff906EF5) ),
      new ItemButton( FontAwesomeIcons.plus, 'Medical Emergency', Color(0xff66A9F2), Color(0xff536CF6) ),
      new ItemButton( FontAwesomeIcons.theaterMasks, 'Theft / Harrasement', Color(0xffF2D572), Color(0xffE06AA3) ),
      new ItemButton( FontAwesomeIcons.biking, 'Awards', Color(0xff317183), Color(0xff46997D) ),
    ];

    List<Widget> itemMap = items.map(
      (item) => FadeInLeft(
        duration: Duration(milliseconds: 250),
        child: FatButtonWidget(
          icon: item.icon,
          color1: item.color1,
          color2: item.color2,
          text: item.texto,
          onPress: (){
            print('asdasdasd');
          }
        ),
      )
    ).toList();
    
    return Scaffold(
      // backgroundColor: Colors.red,
      body: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: (isLarge) ? 220 : 10),
            child: SafeArea(
              child: ListView(
                physics: BouncingScrollPhysics(),
                children: <Widget>[
                   if (isLarge) SizedBox(height: 80,),
                  ...itemMap
                ],
              ),
            ),
          ),
          if (isLarge) _Header(),
        ],
      )
    );
  }
}

class _Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        IconHeader(
          icon: FontAwesomeIcons.plus,
          title: 'Haz solicitado',
          subtitle: 'Asistencia médica',
          color2: Color(0xff66A9F2),
          color1: Color(0xff536CF6),
        ),
        Positioned(
          right: 0,
          top: 45,
          child: RawMaterialButton(
            onPressed: () {},
            shape: CircleBorder(),
            padding: EdgeInsets.all(15),
            child: FaIcon(FontAwesomeIcons.ellipsisV, color: Colors.white),
          ),
        )
      ],
    );
  }
}

class FatButtonTemp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return FatButtonWidget(
      icon: FontAwesomeIcons.carCrash,
      text: 'Motor Accident',
      color1: Color(0xff6989F5),
      color2: Color(0xff906EF5),
      onPress: () {
        print('Fat button clicked :D');
      },
    );
  }
}

class PageHeader extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return IconHeader(
      icon: FontAwesomeIcons.plus,
      title: 'Has solicitado',
      subtitle: 'Asistencia médica',
      color1: Color(0xff526BF6),
      color2: Color(0xff67ACF2),
    );
  }
}