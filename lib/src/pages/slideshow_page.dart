
import 'package:designs/src/theme/theme.dart';
import 'package:flutter/material.dart';

import 'package:designs/src/widgets/slideshow_widget.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class SlideshowPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    bool isLarge = false;
    if (MediaQuery.of(context).size.height > 500) {
      isLarge = true;
    }

    final children = <Widget>[
      Expanded(child: MySlideshow()),
      Expanded(child: MySlideshow()),
    ];

    return Scaffold(
      // backgroundColor: Colors.deepPurple.shade200,
      body: (isLarge)
              ? Column(children: children)
              : Row(children: children)
    );
  }
}

class MySlideshow extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final appTheme = Provider.of<ThemeChanger>(context);
    final accentColor = appTheme.currentTheme.accentColor;

    return SlideshowWidget(
      primaryBullet: 15,
      secondaryBullet: 12,
      dotsTop: false,
      primaryColor: (appTheme.darkTheme) ? accentColor : Colors.pink,
      secondaryColor: Colors.grey,
      slides: <Widget>[
        SvgPicture.asset('assets/svgs/1.svg'),
        SvgPicture.asset('assets/svgs/2.svg'),
        SvgPicture.asset('assets/svgs/3.svg'),
        SvgPicture.asset('assets/svgs/4.svg'),
        SvgPicture.asset('assets/svgs/5.svg'),
      ],
    );
  }
}